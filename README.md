# UnoAsHID #

Make a HID server from Uno

### How to use ###
1. Set DFU mode on arduino, see Hoodloader/*.png
2. Run FLIP, connect thru USB (CTRL+U), load firmware (CTRL+L -> "Hoodloader/Hoodloader1_8.hex"), press Run
3. Replug Arduino, install driver "HoodloaderHID/Hoodloader.inf"
4. Compile and upload "hid_server.ino"
5. Look at "example.py"


### Links ###
* [Atmel FLIP](http://www.atmel.com/ru/ru/tools/flip.aspx)
* [Hoodloader project](https://github.com/NicoHood/Hoodloader)
* [HID for hoodloader project](https://github.com/NicoHood/HID)


### Problems ###
* If FLIP can't open a hex file, try to put it in "C:\sr"
* Driver is unsigned, so a [special mode](https://www.howtogeek.com/167723/how-to-disable-driver-signature-verification-on-64-bit-windows-8.1-so-that-you-can-install-unsigned-drivers/) may be required
* "Platform.h" wasn't found. Get it from here: [https://github.com/arduino/Arduino/blob/1.5.7/hardware/arduino/avr/cores/arduino/Platform.h](https://github.com/arduino/Arduino/blob/1.5.7/hardware/arduino/avr/cores/arduino/Platform.h)
* "hid_client.py" requires [pyserial library](https://pythonhosted.org/pyserial/index.html)