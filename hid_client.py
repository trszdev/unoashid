#!/usr/bin/env python2
import sys, serial, glob

def get_serial_ports():
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')
    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


def int_to_char(x):
    return chr(x if x>=0 else 256+x)

class HidClient:
    MOUSE_LEFT = '\x01'
    MOUSE_RIGHT = '\x02'
    MOUSE_MIDDLE = '\x04'

    def __init__(self, port_str, is_blocking=True):
        self._serial = serial.Serial(
	    port=port_str,
	    baudrate=115200,
	    parity=serial.PARITY_NONE,
	    stopbits=serial.STOPBITS_ONE,
	    bytesize=serial.EIGHTBITS
        )
        self.is_blocking = is_blocking

    def _wait_for_errcode(self):
        if self.is_blocking:
            errcode = self._serial.read()
        
    def press_key(self, code):
        self._serial.write('\x04')
        self._serial.write(code[0])
        self._wait_for_errcode()

    def release_key(self, code):
        self._serial.write('\x01')
        self._serial.write(code[0])
        self._wait_for_errcode()

    def move_mouse(self, dx, dy):
        self._serial.write('\x02')
        self._serial.write(int_to_char(dx))
        self._serial.write(int_to_char(dy))
        self._wait_for_errcode()

    def wheel_mouse(self, dw):
        self._serial.write('\x03')
        self._serial.write(int_to_char(dw))
        self._wait_for_errcode()

    def clear(self):
        self._serial.write('\x00'*6)
        
