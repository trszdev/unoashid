#include <HID.h>

const byte ERR_NONE = 0;
const byte ERR_UNKNOWN_KEY_CODE = 1;
const byte ERR_UNKNOWN_FUNC = 2;

const byte FUNC_PRESS_KEY = 4;
const byte FUNC_RELEASE_KEY = 1;
const byte FUNC_MOVE_MOUSE = 2;
const byte FUNC_WHEEL_MOUSE = 3;
const byte FUNC_CLEAR = 0;

const byte KEY_LMB = 1; // as MOUSE_LEFT, MOUSE_RIGHT, MOUSE_MIDDLE
const byte KEY_RMB = 2;
const byte KEY_WHEEL = 4;

const int MOUSE_STEP = 9; // less to more precision
const int WAIT_DELAY_MS = 50;

/**********************************************************************/

void answer(byte code){
    Serial.print(code, DEC);
}

// relative move, unreliable
void moveMouse(){
    while (Serial.available() < 2)
        delay(WAIT_DELAY_MS);
    char dx = Serial.read();
    char dy = Serial.read();
    char xStep = dx < 0 ? -MOUSE_STEP : MOUSE_STEP;
    char yStep = dy < 0 ? -MOUSE_STEP : MOUSE_STEP;
    for(char i = dx < 0 ? -dx : dx; i > 0; i -= MOUSE_STEP)
        Mouse.move(xStep, 0, 0);
    for(char i = dy < 0 ? -dy : dy; i > 0; i -= MOUSE_STEP)
        Mouse.move(0, yStep, 0);
    //Mouse.move(dx, dy, 0);
    answer(ERR_NONE);
}

void pressKey(){
    while (Serial.available() < 1)
        delay(WAIT_DELAY_MS);
    char code = Serial.read();
    if (code == KEY_LMB || code == KEY_RMB || code == KEY_WHEEL) 
      Mouse.press(code);
    else 
      Keyboard.press(code);
    answer(ERR_NONE); 
}


void releaseKey(){
    while (Serial.available() < 1)
        delay(WAIT_DELAY_MS);
    char code = Serial.read();
    if (code == KEY_LMB || code == KEY_RMB || code == KEY_WHEEL)
      Mouse.release(code);
    else 
      Keyboard.release(code);
    answer(ERR_NONE); 
}

void wheelMouse(){
    while (Serial.available() < 2)
        delay(WAIT_DELAY_MS);
    char dw = Serial.read();
    Mouse.move(0, 0, dw);
    answer(ERR_NONE);
}

/**********************************************************************/

void serialReadToEnd() {
    while (Serial.available() > 0)
        Serial.read();
}

void setup() {
    Serial.begin(SERIAL_HID_BAUD);
    serialReadToEnd();
    Mouse.begin();
    Keyboard.begin();
}


void loop() {
    if (Serial.available() > 0) {
        byte funcCode = Serial.read();
             if (funcCode == FUNC_PRESS_KEY) pressKey();
        else if (funcCode == FUNC_RELEASE_KEY) releaseKey();
        else if (funcCode == FUNC_MOVE_MOUSE) moveMouse();
        else if (funcCode == FUNC_WHEEL_MOUSE) wheelMouse();
        else if (funcCode == FUNC_CLEAR) serialReadToEnd();
    }
}
