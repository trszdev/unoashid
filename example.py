import hid_client as hc
import time

available_ports = hc.get_serial_ports()
print "Available serial ports: ", available_ports 
port = available_ports[-1]
print "I'll use ", port

client = hc.HidClient(port)
client.clear() # ensure there is no overlap
print "After 3 seconds I'll print hello world"
time.sleep(3)

for x in 'hello world':
    client.press_key(x)
    time.sleep(0.02)
    client.release_key(x)

print "After 3 seconds I'll move cursor 30px right and 120px down"
time.sleep(3)
client.move_mouse(30, 120)


print "After 3 seconds I'll move cursor 30px left and 120px up"
time.sleep(3)
client.move_mouse(-30, -120)
